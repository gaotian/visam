<?php

namespace AppBundle\Component\Duplication;

class Duplication
{
    public function duplicateEtablissementItems($em, $id)
    {
        // dernière année & année en cours
        $anneeCible = $em->getRepository('AppBundle:Collecte')->findCollecte();
        $anneeSource = $anneeCible - 1;
        $now = new \DateTime();

        // tous les labos d'un établissement pour la dernière annéee
        $query = $em->createQuery(
            'SELECT l FROM AppBundle:Labo l JOIN l.etablissement e WHERE l.anneeCollecte = :annee AND e.etablissementId  = :etablissementId'
        );
        $query->setParameter('annee', $anneeSource);
        $query->setParameter('etablissementId', $id);
        $labos = $query->getResult();

        //toutes les formations d'un établissement pour la dernière année
        $query = $em->createQuery(
            'SELECT f FROM AppBundle:Formation f JOIN f.etablissement e WHERE f.anneeCollecte = :annee AND e.etablissementId = :etablissementId'
        );
        $query->setParameter('annee', $anneeSource);
        $query->setParameter('etablissementId', $id);
        $formations = $query->getResult();

        //Duplication de chaque élément
        foreach ($labos as $labo) {

            $id = $labo->getId();
            $labo->setAnneeCollecte($anneeCible);
            $labo->setDateCreation($now);
            $labo->setLastUpdate($now);

            //retrouver l'établissement du labo
            $query = $em->createQuery(
                'SELECT e.etablissementId FROM AppBundle:Etablissement e JOIN e.labo l WHERE l.id = :id '
            );
            $query->setParameter('id', $id);
            $etablissements = $query->getResult();
            $repository = $em->getRepository('AppBundle:Etablissement');

            foreach ($etablissements as $item){
                $etablissement = $repository->findOneByEtablissementId($item['etablissementId']);
                $etablissement->addLabo($labo);
            }

            //Correction des disciplines

            $query = $em->createQuery(
                'SELECT d FROM AppBundle:Discipline d JOIN d.labo l WHERE d.type = :type AND l.id = :id'
            );
            $query->setParameter('type', 'cnu');
            $query->setParameter('id', $id);
            $cnu = $query->getResult();
            $labo->setCnu($cnu);

            $query = $em->createQuery(
                'SELECT d FROM AppBundle:Discipline d JOIN d.labo l WHERE d.type = :type AND l.id = :id'
            );
            $query->setParameter('type', 'sise');
            $query->setParameter('id', $id);
            $sise = $query->getResult();
            $labo->setSise($sise);

            $query = $em->createQuery(
                'SELECT d FROM AppBundle:Discipline d JOIN d.labo l WHERE d.type = :type AND l.id = :id'
            );
            $query->setParameter('type', 'hceres');
            $query->setParameter('id', $id);
            $hceres = $query->getResult();
            $labo->setHceres($hceres);

            $em->detach($labo);
            $em->persist($labo);
        }

        foreach ($formations as $formation) {

            $id = $formation->getId();
            $formation->setAnneeCollecte($anneeCible);
            $formation->setDateCreation($now);
            $formation->setLastUpdate($now);

            //retrouver l'établissement d'une formation
            $query = $em->createQuery(
                'SELECT e.etablissementId FROM AppBundle:Etablissement e JOIN e.formation l WHERE l.id = :id '
            );
            $query->setParameter('id', $id);
            $etablissements = $query->getResult();

            $repository = $em->getRepository('AppBundle:Etablissement');

            foreach ($etablissements as $item){
                $etablissement = $repository->findOneByEtablissementId($item['etablissementId']);
                $etablissement->addFormation($formation);
            }

            //Correction des disciplines
            $query = $em->createQuery(
                'SELECT d FROM AppBundle:Discipline d JOIN d.formation f WHERE d.type = :type AND f.id = :id'
            );
            $query->setParameter('type', 'cnu');
            $query->setParameter('id', $id);
            $cnu = $query->getResult();
            $formation->setCnu($cnu);

            $query = $em->createQuery(
                'SELECT d FROM AppBundle:Discipline d JOIN d.formation f WHERE d.type = :type AND f.id = :id'
            );
            $query->setParameter('type', 'sise');
            $query->setParameter('id', $id);
            $sise = $query->getResult();
            $formation->setSise($sise);

            $query = $em->createQuery(
                'SELECT d FROM AppBundle:Discipline d JOIN d.formation f WHERE d.type = :type AND f.id = :id'
            );
            $query->setParameter('type', 'hceres');
            $query->setParameter('id', $id);
            $hceres = $query->getResult();
            $formation->setHceres($hceres);

            $em->detach($formation);
            $em->persist($formation);

        }

        $em->flush();

        return true;
    }

    public function duplicateEtablissementsItems()
    {
        return true;
    }
}