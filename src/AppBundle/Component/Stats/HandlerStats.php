<?php

namespace AppBundle\Component\Stats;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HandlerStats extends Controller
{
    public function generalStats($em, $anneeCollecte)
    {
        $stats = array();

        $query = $em->createQuery('SELECT COUNT(e) as nb FROM AppBundle:Ed e WHERE e.anneeCollecte = :annee');
        $query->setParameter('annee', $anneeCollecte);
        $stats['nb_eds'] = $query->getSingleResult();

        $query = $em->createQuery('SELECT COUNT(e) as nb FROM AppBundle:Formation e WHERE e.anneeCollecte = :annee');
        $query->setParameter('annee', $anneeCollecte);
        $stats['nb_formations'] = $query->getSingleResult();

        $query = $em->createQuery('SELECT COUNT(e) as nb FROM AppBundle:Labo e WHERE e.anneeCollecte = :annee');
        $query->setParameter('annee', $anneeCollecte);
        $stats['nb_laboratoires'] = $query->getSingleResult();

        return $stats;
    }

}