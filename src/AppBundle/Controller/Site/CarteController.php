<?php

namespace AppBundle\Controller\Site;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/carte")
 */
class CarteController extends Controller
{
    /**
     *
     * @Route("/", name="carte")
     */
    public function indexAction()
    {
      $em = $this->getDoctrine()->getManager();

      $anneeCollecte = $em->getRepository('AppBundle:Collecte')->findDerniereCollecte();
        return $this->render('@App/search/carte.html.twig', array(
          'anneeCollecte' => $anneeCollecte,

        ));
    }


}
