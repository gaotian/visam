<?php

namespace AppBundle\Controller\Site;

use AppBundle\Component\Stats\HandlerStats;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\LayoutHydrator;


class HomepageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $anneeCollecte = $em->getRepository('AppBundle:Collecte')->findDerniereCollecte();
        $hesamettes = $em->getRepository('AppBundle:Hesamette')->findAll();

        $stats = (new HandlerStats())->generalStats($em, $anneeCollecte);

        $query = $em->createQuery('SELECT e FROM AppBundle:Etablissement e JOIN e.collecte c WHERE  c.annee = :annee');
        $query->setParameter('annee', $anneeCollecte);
        $etabs = $query->getResult();

        return $this->render('web/index.html.twig', array(
            'hesamettes' => $hesamettes,
            'stats' => $stats,
            'etabs' => $etabs,

        ));
    }

}
