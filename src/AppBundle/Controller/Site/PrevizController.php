<?php

namespace AppBundle\Controller\Site;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/previz")
 */
class PrevizController extends Controller
{
    /**
     *
     * @Route("/", name="previz")
     */
    public function indexAction()
    {
        return $this->render('@App/search/previz.html.twig', array(

        ));
    }

    /**
     * @Route("/iframe", name="search_iframe")
     */
    public function iframeAction()
    {
        return $this->render('@App/search/iframe.html.twig', array(

        ));
    }

}
