<?php

namespace AppBundle\Controller\Stats;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;



class StatistiquesController extends Controller
{
    /**
     * @Route("/explorer", name="statistiques")
     */
    public function indexAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();

        $anneeCollecte = $em->getRepository('AppBundle:Collecte')->findDerniereCollecte();

        $query = $em->createQuery('SELECT e FROM AppBundle:Ed e WHERE e.anneeCollecte = :annee');
        $query->setParameter('annee', $anneeCollecte);
        $eds = $query->getResult();

        $query = $em->createQuery('SELECT e FROM AppBundle:Formation e WHERE e.anneeCollecte = :annee');
        $query->setParameter('annee', $anneeCollecte);
        $formations = $query->getResult();

        $query = $em->createQuery('SELECT e FROM AppBundle:Labo e WHERE e.anneeCollecte = :annee');
        $query->setParameter('annee', $anneeCollecte);
        $labos = $query->getResult();

        $query = $em->createQuery('SELECT e FROM AppBundle:Etablissement e JOIN e.collecte c WHERE  c.annee = :annee AND e.active = 1');
        $query->setParameter('annee', $anneeCollecte);
        $etabs = $query->getResult();

        $disciplines = $em->getRepository('AppBundle:Discipline')->findAll();

        $hesamette = $em->getRepository('AppBundle:hesamette')->findAll();


        $nbEtud = 125464;

        //calculer la répartition des niveaux dans les formations
        $query = $em->createQuery('SELECT n.niveau, COUNT(n.niveau) AS nb FROM AppBundle:Formation n WHERE n.anneeCollecte = :annee GROUP BY n.niveau  ORDER BY nb DESC');
        $query->setParameter('annee', $anneeCollecte);
        $nbFormationsNiveau = $query->getResult();

//Les Hésamettes

        //nombre de formations par hesamettes
        $query = $em->createQuery('SELECT h.nom as hesamette, COUNT(f) as nb, f.nom as formation FROM AppBundle:Discipline d JOIN d.formation f JOIN d.hesamette h WHERE f.anneeCollecte = :annee GROUP BY h ORDER BY nb DESC');
        $query->setParameter('annee', $anneeCollecte);
        $formationsHesamette = $query->getResult();

        //répartition des hesamettes par labos
        $query = $em->createQuery('SELECT h.nom as hesamette, COUNT(l) as nb, l.nom as labo FROM AppBundle:Discipline d JOIN d.labo l JOIN d.hesamette h WHERE l.anneeCollecte = :annee GROUP BY h ORDER BY nb DESC');
        $query->setParameter('annee', $anneeCollecte);
        $labosHesamette = $query->getResult();

//Disciplines des formations

        //récupérer toutes les formations, leurs disciplines et le nombre de disciplines liées
        $query = $em->createQuery('SELECT d as item, COUNT(f.nom) as nb, f.nom as formation, f.id as id FROM AppBundle:Discipline d JOIN d.formation f WHERE f.anneeCollecte = :annee GROUP BY d ORDER BY nb DESC')->setMaxResults(20);
        $query->setParameter('annee', $anneeCollecte);
        $allDisciplinesFormations = $query->getResult();

        //récupérer toutes les formations, leurs disciplines et le nombre de disciplines liées NW3
        $query = $em->createQuery('SELECT d as item, COUNT(f.nom) as nb, f.nom as formation, f.id as id FROM AppBundle:Discipline d JOIN d.formation f WHERE d.type=:type  AND f.anneeCollecte = :annee GROUP BY d ORDER BY nb DESC')->setMaxResults(20);
        $query->setParameter('type', 'NW3');
        $query->setParameter('annee', $anneeCollecte);
        $allNw3DisciplinesFormations = $query->getResult();

//Disciplines des labos

        //récupérer tous les labos, leurs disciplines et le nombre de disciplines liées
        $query = $em->createQuery('SELECT d as item, COUNT(f.nom) as nb, f.nom as labo, f.id as id FROM AppBundle:Discipline d JOIN d.labo f WHERE f.anneeCollecte = :annee GROUP BY d ORDER BY nb DESC')->setMaxResults(20);
        $query->setParameter('annee', $anneeCollecte);
        $allDisciplinesLabos = $query->getResult();


        //récupérer tous les labos, leurs disciplines et le nombre de disciplines liées NW3
        $query = $em->createQuery('SELECT d as item, COUNT(f.nom) as nb, f.nom as labo, f.id as id FROM AppBundle:Discipline d JOIN d.labo f WHERE d.type=:type AND f.anneeCollecte = :annee GROUP BY d ORDER BY nb DESC')->setMaxResults(20);
        $query->setParameter('type', 'NW3');
        $query->setParameter('annee', $anneeCollecte);
        $allNw3DisciplinesLabos = $query->getResult();


// ------------------------------------------------------------------------
// ------------------------------------ labo ------------------------------------
// ------------------------------------------------------------------------

        //nb de types pour tous les labos
        $query = $em->createQuery('SELECT l.type as type, COUNT(l.nom) as nb FROM AppBundle:Labo l WHERE l.type != :type AND l.anneeCollecte = :annee GROUP BY l.type ORDER BY nb DESC');
        $query->setParameter('type', '');
        $query->setParameter('annee', $anneeCollecte);
        $nbTypeLabos = $query->getResult();



        return $this->render('web/stats.html.twig', array(
        	'eds' => $eds,
        	'etabs' => $etabs,
            'nbEtud' => $nbEtud,
            'formations' => $formations,
            'nbFormationsNiveau'=> $nbFormationsNiveau,
            'allDisciplinesFormations'=>$allDisciplinesFormations,
            'allNw3DisciplinesFormations'=>$allNw3DisciplinesFormations,
            'formationsHesamette' => $formationsHesamette,
            //Labos>Disciplines
            'allDisciplinesLabos'=>$allDisciplinesLabos,
            'allNw3DisciplinesLabos'=>$allNw3DisciplinesLabos,
            'labosHesamette' =>$labosHesamette,
            //labos
            'nbTypeLabos' => $nbTypeLabos,
            //entités
            'labos' => $labos,
            'disciplines' => $disciplines
        	));
    }


} // Fin de la class DefaultController
