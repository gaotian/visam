<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CollecteRepository extends EntityRepository
{
    public function findCollecteActive()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c.annee FROM AppBundle:Collecte c WHERE c.active = 1'
            )
            ->getResult();
    }

    public function findDerniereCollecte()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c.annee FROM AppBundle:Collecte c WHERE c.complete = 1 ORDER BY c.annee DESC'
            )
            ->setMaxResults(1)
            ->getResult();
    }

    public function findCollecte(){
        $query = $this->getEntityManager()->createQuery('
        SELECT c.annee FROM AppBundle:Collecte c WHERE c.active = 1
        ');
        $active = $query->getResult();

        $query = $this->getEntityManager()->createQuery('
        SELECT c.annee FROM AppBundle:Collecte c WHERE c.complete = 1 ORDER BY c.annee DESC
        ');
        $complete = $query->getResult();

        if($active){
            return $active[0]['annee'] ;
        }
        else if(!$active && $complete){
            return $complete[0]['annee'];
        }
        else{
            throw new \Exception("Il n'y a pas de collecte");
        }

    }

    public function infos()
    {
        $result = [];

        $query = $this->getEntityManager()->createQuery('SELECT COUNT(c) as nb FROM AppBundle:Collecte c');
        $nb_collecte = $query->getSingleResult();
        $result['total']= $nb_collecte['nb'];

        $query = $this->getEntityManager()->createQuery('SELECT COUNT(c) as nb FROM AppBundle:Collecte c WHERE c.complete = 1');
        $nb_complete_collecte = $query->getSingleResult();
        $result['nb_complete']= $nb_complete_collecte['nb'];

        $query = $this->getEntityManager()->createQuery('SELECT c.annee as nom FROM AppBundle:Collecte c WHERE c.active = 1');
        $active = $query->getSingleResult();
        $result['nom_active'] = $active['nom'];

        $query = $this->getEntityManager()->createQuery('SELECT c.annee as nom FROM AppBundle:Collecte c WHERE c.complete = 1 ORDER BY c.annee DESC')->setMaxResults(1);
        $last_collecte = $query->getSingleResult();
        $result['last'] = $last_collecte['nom'];



        return $result;
    }

}