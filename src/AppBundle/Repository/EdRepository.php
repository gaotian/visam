<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EdRepository extends EntityRepository
{
    public function getAllEcolesDoctorales($id)
    {
        $qb = $this->createQueryBuilder('ed')
            ->select('ed.code')
            ->leftJoin('ed.labo', 'l')
            ->where('l.id = :labo')
            ->setParameter('labo', $id);

        $query = $qb->getQuery()->getArrayResult();

        return $query;
    }

    public function infos()
    {
        $result = [];

        $query = $this->getEntityManager()->createQuery('SELECT COUNT(f) as nb FROM AppBundle:Ed f');
        $nb_formations = $query->getSingleResult();
        $result['total']= $nb_formations['nb'];


        $query = $this->getEntityManager()->createQuery(
            'SELECT f.anneeCollecte as annee, COUNT(f) as nb FROM AppBundle:Ed f GROUP BY f.anneeCollecte'
        );
        $result['by_collecte'] = $query->getResult();


        $query = $this->getEntityManager()->createQuery(
            'SELECT e.nom as etablissement, COUNT(f) as nb FROM AppBundle:Ed f JOIN f.etablissement e GROUP BY e.etablissementId ORDER BY nb DESC'
        );
        $result['by_etablissement'] = $query->getResult();

        return $result;
    }
}