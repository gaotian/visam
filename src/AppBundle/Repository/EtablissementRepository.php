<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EtablissementRepository extends EntityRepository
{

    public function createAlphabeticalQueryBuilder()
    {
        return $this->createQueryBuilder('etablissement')
            ->orderBy('etablissement.nom', 'ASC');
    }

    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT e FROM AppBundle:Etablissement e ORDER BY e.nom ASC'
            )
            ->getResult();
    }

    public function findAllActif()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT e FROM AppBundle:Etablissement e WHERE e.active = 1 ORDER BY e.nom ASC'
            )
            ->getResult();
    }

    public function findAllByCollecteActive()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT e FROM AppBundle:Etablissement e JOIN e.collecte c WHERE e.active = 1  AND c.active = 1 ORDER BY e.nom ASC'
            )
            ->getResult();
    }

    public function findAllLabo($etablissementId)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT l FROM AppBundle:Labo l INNER JOIN l.etablissement e WHERE l.anneeCollecte = :annee AND e.etablissementId = :etablissementId'
            )
            ->setParameter('etablissementId', $etablissementId)
            ->getResult();

    }

    public function findAllFormations($etablissementId)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f FROM AppBundle:Formation f INNER JOIN f.etablissement e WHERE f.anneeCollecte = :annee AND e.etablissementId = :etablissementId'
            )
            ->setParameter('etablissementId', $etablissementId)
            ->getResult();
    }

    public function findAllEds($etablissementId)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f FROM AppBundle:Ed f INNER JOIN f.etablissement e WHERE f.anneeCollecte = :annee AND e.etablissementId = :etablissementId'
            )
            ->setParameter('etablissementId', $etablissementId)
            ->getResult();
    }

    // Par établissement

    public function findAllLaboByCollecteByEtab($etablissementId, $anneeCollecte)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT l FROM AppBundle:Labo l INNER JOIN l.etablissement e WHERE l.anneeCollecte = :annee AND e.etablissementId = :etablissementId'
            )
            ->setParameter('etablissementId', $etablissementId)
            ->setParameter('annee', $anneeCollecte)
            ->getResult();
    }

    public function findAllFormationsByCollecteByEtab($etablissementId, $anneeCollecte)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f FROM AppBundle:Formation f INNER JOIN f.etablissement e WHERE f.anneeCollecte = :annee AND e.etablissementId = :etablissementId'
            )
            ->setParameter('etablissementId', $etablissementId)
            ->setParameter('annee', $anneeCollecte)
            ->getResult();
    }

    public function findAllEdsByCollecteByEtab($etablissementId, $anneeCollecte)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f FROM AppBundle:Ed f INNER JOIN f.etablissement e WHERE f.anneeCollecte = :annee AND e.etablissementId = :etablissementId'
            )
            ->setParameter('etablissementId', $etablissementId)
            ->setParameter('annee', $anneeCollecte)
            ->getResult();
    }

    // Tous les items de tous les établissements

    /**
     * @param $anneeCollecte
     * @return mixed
     */
    public function findAllLabosByCollecte($anneeCollecte)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT l FROM AppBundle:Labo l INNER JOIN l.etablissement e WHERE l.anneeCollecte = :annee AND e.active = 1'
            )
            ->setParameter('annee', $anneeCollecte)
            ->getResult();
    }

    /**
     * @param $anneeCollecte
     * @return mixed
     */
    public function findAllFormationsByCollecte($anneeCollecte)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f FROM AppBundle:Formation f INNER JOIN f.etablissement e WHERE f.anneeCollecte = :annee  AND e.active = 1'
            )
            ->setParameter('annee', $anneeCollecte)
            ->getResult();
    }

    /**
     * @param $anneeCollecte
     * @return mixed
     */
    public function findAllEdsByCollecte($anneeCollecte)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT f FROM AppBundle:Ed f INNER JOIN f.etablissement e WHERE f.anneeCollecte = :annee  AND e.active = 1'
            )
            ->setParameter('annee', $anneeCollecte)
            ->getResult();
    }


    //Tous les items de tous les établissements d'un contributeur

    public function findAllByOneContributeur($contributeurId)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT e FROM AppBundle:Etablissement e JOIN e.contributeur u WHERE e.active = 1 AND u.id = :contributeurId ORDER BY e.nom ASC'
            )
            ->setParameter('contributeurId', $contributeurId)
            ->getResult();
    }

    public function infos(){

        $result = [];

        $query = $this->getEntityManager()->createQuery('SELECT COUNT(e) as nb FROM AppBundle:Etablissement e');
        $nb_etab = $query->getSingleResult();
        $result['total']= $nb_etab['nb'];

        $query = $this->getEntityManager()->createQuery('SELECT COUNT(e) as nb FROM AppBundle:Etablissement e WHERE e.active = 1');
        $nb_active_etab = $query->getSingleResult();
        $result['active'] = $nb_active_etab['nb'];

        return $result;

    }

    /**
     * @param $anneeCollecte
     * @return mixed
     */
    public function findAllLabosByCollecteAndUser($anneeCollecte, $contributeurId)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT l FROM AppBundle:Labo l INNER JOIN l.etablissement e INNER JOIN e.contributeur c WHERE l.anneeCollecte = :annee AND c.id = :contributeurId'
            )
            ->setParameter('annee', $anneeCollecte)
            ->setParameter('contributeurId', $contributeurId)
            ->getResult();
    }

    /**
     * @param $anneeCollecte
     * @return mixed
     */
    public function findAllFormationsByCollecteAndUser($anneeCollecte, $contributeurId)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT l FROM AppBundle:Formation l INNER JOIN l.etablissement e  INNER JOIN e.contributeur c WHERE l.anneeCollecte = :annee AND c.id = :contributeurId'
            )
            ->setParameter('annee', $anneeCollecte)
            ->setParameter('contributeurId', $contributeurId)
            ->getResult();
    }


    /**
     * @param $anneeCollecte
     * @return mixed
     */
    public function findAllEdsByCollecteAndUser($anneeCollecte, $contributeurId)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT l FROM AppBundle:Ed l INNER JOIN l.etablissement e  INNER JOIN e.contributeur c WHERE l.anneeCollecte = :annee AND c.id = :contributeurId'
            )
            ->setParameter('annee', $anneeCollecte)
            ->setParameter('contributeurId', $contributeurId)
            ->getResult();
    }

    /**
     * @param $id, $code
     * @return boolean
     *
     */
    public function verifyEtablissementByCodeAndId($id, $code)
    {
        $etablissement = $this
            ->findOneBy(array('etablissementId' => $id, 'code' => $code));

        if (!$etablissement) {
            return false;
        }
        return true;
    }
}

