<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Etablissement;

class FormationRepository extends EntityRepository
{
    public function findAllFormationsApi()
    {

        $qb = $this->createQueryBuilder('f')
            ->join('f.discipline', 'd')
            ->select('f, d.nom as discipline') //une seule discipline est gardé pour l'instant
            ->addOrderBy('f.nom', 'ASC');


        $query = $qb->getQuery()->getArrayResult();

        return $query;

    }

    public function findAllFormations(Etablissement $etablissement)
    {
        $qb = $this->createQueryBuilder('f')
            ->select('f')
            ->where('e.etablissement = :etab')
            ->setParameter('etab', $etablissement);

        $query = $qb->getQuery()->getArrayResult();

        return $query;
    }

    public function infos()
    {
        $result = [];

        $query = $this->getEntityManager()->createQuery('SELECT COUNT(f) as nb FROM AppBundle:Formation f');
        $nb_formations = $query->getSingleResult();
        $result['total']= $nb_formations['nb'];

        $query = $this->getEntityManager()->createQuery('SELECT COUNT(DISTINCT(f.objetId)) as nb FROM AppBundle:Formation f');
        $nb_distinct = $query->getSingleResult();
        $result['distinct'] = $nb_distinct['nb'];

        $query = $this->getEntityManager()->createQuery(
            'SELECT f.anneeCollecte as annee, COUNT(f) as nb FROM AppBundle:Formation f GROUP BY f.anneeCollecte'
        );
        $result['by_collecte'] = $query->getResult();

        $query = $this->getEntityManager()->createQuery(
            'SELECT e.nom as etablissement, COUNT(f) as nb FROM AppBundle:Formation f JOIN f.etablissement e GROUP BY e.etablissementId ORDER BY nb DESC'
        );
        $result['by_etablissement'] = $query->getResult();

        $query = $this->getEntityManager()->createQuery(
            'SELECT f.id FROM AppBundle:Formation f JOIN f.etablissement e'
        );
        $formationEtablissement = $query->getResult();

        //nb de formations sans établissement
        $query = $this->getEntityManager()->createQuery('
            SELECT f FROM AppBundle:Formation f WHERE f.id NOT IN (:formations)
        ')
        ->setParameter('formations', $formationEtablissement);
        $result['no_etablissement'] = $query->getResult();

        $query = $this->getEntityManager()->createQuery('
            SELECT COUNT(f) as nb FROM AppBundle:Formation f WHERE f.id NOT IN (:formations)
        ')
            ->setParameter('formations', $formationEtablissement);
        $nb_orphelins = $query->getSingleResult();
        $result['nb_orphelins'] = $nb_orphelins['nb'];

        return $result;
    }
}

