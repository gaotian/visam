<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class LaboRepository extends EntityRepository
{
    public function findAllLabos(){

        $qb = $this->createQueryBuilder('labo')
            ->addOrderBy('labo.nom', 'ASC');

        $query = $qb->getQuery()->getArrayResult();

        return $query;
    }

    public function infos()
    {
        $result = [];

        $query = $this->getEntityManager()->createQuery('SELECT COUNT(f) as nb FROM AppBundle:Labo f');
        $nb_formations = $query->getSingleResult();
        $result['total']= $nb_formations['nb'];

        $query = $this->getEntityManager()->createQuery('SELECT COUNT(DISTINCT(f.objetId)) as nb FROM AppBundle:Labo f');
        $nb_distinct = $query->getSingleResult();
        $result['distinct'] = $nb_distinct['nb'];

        $query = $this->getEntityManager()->createQuery(
            'SELECT f.anneeCollecte as annee, COUNT(f) as nb FROM AppBundle:Labo f GROUP BY f.anneeCollecte'
        );
        $result['by_collecte'] = $query->getResult();


        $query = $this->getEntityManager()->createQuery(
            'SELECT e.nom as etablissement, COUNT(f) as nb FROM AppBundle:Labo f JOIN f.etablissement e GROUP BY e.etablissementId ORDER BY nb DESC'
        );
        $result['by_etablissement'] = $query->getResult();

        $query = $this->getEntityManager()->createQuery(
            'SELECT f.id FROM AppBundle:Labo f JOIN f.etablissement e'
        );
        $laboEtablissement = $query->getResult();

        //nb de labo sans établissement
        $query = $this->getEntityManager()->createQuery('
            SELECT f FROM AppBundle:Labo f WHERE f.id NOT IN (:laboratoires)
        ')
            ->setParameter('laboratoires', $laboEtablissement);
        $result['no_etablissement'] = $query->getResult();

        $query = $this->getEntityManager()->createQuery('
            SELECT COUNT(f) as nb FROM AppBundle:Labo f WHERE f.id NOT IN (:laboratoires)
        ')
            ->setParameter('laboratoires', $laboEtablissement);
        $nb_orphelins = $query->getSingleResult();
        $result['nb_orphelins'] = $nb_orphelins['nb'];

        return $result;
    }
}
