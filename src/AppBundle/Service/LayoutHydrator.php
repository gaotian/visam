<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LayoutHydrator extends Controller
{

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function hydrateFooter()
    {
        $etablissements = $this->em->getRepository('AppBundle:Etablissement')->findAllOrderedByName();

        return $etablissements;
    }
}