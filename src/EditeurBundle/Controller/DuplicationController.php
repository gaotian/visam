<?php

namespace EditeurBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Component\Duplication\Duplication;

class DuplicationController extends Controller
{
    /**
     * @param $etablissementId
     * @Route("/admin/duplication/etablissement/{etablissementId}", name="duplication_etablissement")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($etablissementId)
    {
        $em = $this->getDoctrine()->getManager();

        $duplication = new Duplication();
        $duplication->duplicateEtablissementItems($em, $etablissementId);

        $this->addFlash('success', "Les données de l'établissement ont bien été dupliquées");
        return $this->redirectToRoute('admin');
    }
}
