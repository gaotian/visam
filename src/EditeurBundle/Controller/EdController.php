<?php

namespace EditeurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


use AppBundle\Entity\Ed;
use EditeurBundle\Form\EdType;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 * @Route("/editeur")
 */
class EdController extends Controller
{

    /**
     * Lists all Ed entities.
     *
     * @Route("/ed", name="editeur_ed_index")
     * @Method("GET")
     */
    public function allEdAction()
    {
        $em = $this->getDoctrine()->getManager();

        $eds = $em->getRepository('AppBundle:Ed')->findAll();

        return $this->render('EditeurBundle:Ed:index.html.twig', array(
            'eds' => $eds,
        ));
    }

    /**
     * Créer une nouvelle Ed
     *
     * @Route("/ed/new", name="editeur_ed_new")
     * @Method({"GET", "POST"})
     */
    public function newEdAction(Request $request)
    {
        $ed = new Ed();

        $em = $this->getDoctrine()->getManager();

        //ajout des établissements pour le formulaire
        $user = $this->getUser();
        if ($user->hasRole('ROLE_ADMIN')){
            $query = $em->createQuery(
                'SELECT e.etablissementId as id FROM AppBundle:Etablissement e'
            );
        }

        else{
            $userId = $user->getId();
            $query = $em->createQuery(
                'SELECT e.etablissementId as id FROM AppBundle:User u INNER JOIN u.etablissement e WHERE u.id = :user'
            );
            $query->setParameter('user', $userId);
        }
        $etablissements = $query->getResult();

        $query = $em->createQuery(
            'SELECT l.localisationId as id FROM AppBundle:Localisation l JOIN l.etablissement as e WHERE e.etablissementId IN (:etablissements)'
        );
        $query->setParameter('etablissements', $etablissements);
        $localisations = $query->getResult();

        $year = $em->getRepository('AppBundle:Collecte')->findDerniereCollecte();
        $year = $year[0]['annee'];

        $editForm = $this->createForm('EditeurBundle\Form\EdType', $ed, array(
            'etablissements' => $etablissements,
            'localisations' => $localisations
        ));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            // --------------------------
            //Ajout des établissements
            // --------------------------

            //s'il n'y en a qu'un, ajout automatique
            if(count($etablissements) == 1){

                $repository = $this->getDoctrine()->getRepository('AppBundle:Etablissement');
                $etablissement = $repository->findOneByEtablissementId($etablissements[0]['id']);

                $ed->addEtablissement($etablissement);
            }
            //sinon l'utilisateur choisit lui-même un établissement
            $ed->setAnneeCollecte($year);

            $now = new \DateTime();
            $ed->setDateCreation($now);
            $ed->setLastUpdate($now);

            $em->persist($ed);
            $em->flush();

            //Création et set de l'objetId
            $lastId = $ed->getEdId();
            $ed->setObjetId("E".$lastId);

            $em->persist($ed);
            $em->flush();

            $this->addFlash(
                'success',
                "L'école doctorale a bien été sauvegardée"
            );
            return $this->redirectToRoute('editeur');

        }

        return $this->render('EditeurBundle:Ed:new.html.twig', array(
            'ed' => $ed,
            'form' => $editForm->createView(),
            'etablissements' => $etablissements,
            'localisations' => $localisations
        ));
    }

    /**
     * Finds and displays a Ed entity.
     *
     * @Route("/ed/{id}", name="editeur_ed_show")
     * @Method("GET")
     */
    public function showEdAction(Ed $ed)
    {
        $deleteForm = $this->createDeleteEdForm($ed);

        return $this->render('EditeurBundle:Ed:show.html.twig', array(
            'ed' => $ed,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Ed entity.
     *
     * @Route("/ed/{id}/edit", name="editeur_ed_edit")
     * @Method({"GET", "POST"})
     */
    public function editEdAction(Request $request, Ed $ed)
    {

        $deleteForm = $this->createDeleteEdForm($ed);

        $em = $this->getDoctrine()->getManager();

        //ajout des établissements pour le formulaire
        $user = $this->getUser();

        if ($user->hasRole('ROLE_ADMIN')){
            $query = $em->createQuery(
                'SELECT e.etablissementId as id FROM AppBundle:Etablissement e'
            );
        }

        else{
            $userId = $user->getId();
            $query = $em->createQuery(
                'SELECT e.etablissementId as id FROM AppBundle:User u INNER JOIN u.etablissement e WHERE u.id = :user'
            );
            $query->setParameter('user', $userId);
        }
        $etablissements = $query->getResult();

        //vérification que l'utilisateur peut modifier cette ed

        //Sélection de tous les établissements rattachés à la ed
        $query = $em->createQuery("SELECT e.etablissementId as id FROM AppBundle:Etablissement e JOIN e.ed f WHERE f.edId = :id");
        $query->setParameter('id', $ed->getEdId());
        $etab_user = $query->getResult();

        //vérification que les établissement de l'ed sont bien dans ceux du user

        $checkUser = [];

        if ($user->hasRole('ROLE_ADMIN')){
            $checkUser = ['all'];
        }
        else{
            for ($i = 0; $i < count($etablissements); $i++){

                for($j = 0; $j < count($etab_user);$j++){
                    if($etablissements[$i] == $etab_user[$j]){
                        array_push($checkUser,$etab_user[$j]);
                    }

                }
            }
        }

        //si l'utilisateur a l'établissement de l'ed dans sa liste
        if(count($checkUser) > 0) {

            $query = $em->createQuery(
                'SELECT l.localisationId as id FROM AppBundle:Localisation l JOIN l.etablissement as e WHERE e.etablissementId IN (:etablissements)'
            );
            $query->setParameter('etablissements', $etablissements);
            $localisations = $query->getResult();

            $editForm = $this->createForm('EditeurBundle\Form\EdType', $ed, array(
                'etablissements' => $etablissements,
                'localisations' => $localisations
            ));

            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {

                $em = $this->getDoctrine()->getManager();

                $now = new \DateTime();
                $ed->setLastUpdate($now);

                $em->persist($ed);
                $em->flush();

                $this->addFlash(
                    'success',
                    "Les changements ont été sauvegardés!"
                );

                return $this->redirectToRoute('editeur');

            }

            return $this->render('EditeurBundle:Ed:edit.html.twig', array(
                'ed' => $ed,
                'form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                'etablissements' => $etablissements,
                'localisations' => $localisations
            ));
        }
        else{
            $this->addFlash('success', "Vous ne pouvez modifier cette écolde doctorale, vous n'êtes pas rattaché à l'établissement auquelle elle appartient");
            return $this->redirectToRoute('editeur_ed_edit', array('id' => $ed->getEdId()));
        }

    }

    /**
     * Fonction pour effacer via ajax une ed
     *
     * @Route("/delete/{id}", name="editeur_ed_ajax_delete")
     * @Method("DELETE")
     */
    public function deleteAjaxAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($user->hasRole('ROLE_USER')){

            /** @var Ed $ed */
            $ed = $em->getRepository('AppBundle:Ed')
                ->find($id);
            $em->remove($ed);
            $em->flush();
        }

        return new Response(null, 204);
    }


    /**
     * Deletes a Ed entity.
     *
     * @Route("/ed/{id}", name="editeur_ed_delete")
     * @Method("DELETE")
     */
    public function deleteEdAction(Request $request, Ed $ed)
    {
        $form = $this->createDeleteEdForm($ed);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ed);
            $em->flush();
        }

        return $this->redirectToRoute('editeur_ed_index');
    }

    /**
     * Creates a form to delete a Ed entity.
     *
     * @param Ed $ed The Ed entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteEdForm(Ed $ed)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('editeur_ed_delete', array('id' => $ed->getEdId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}
