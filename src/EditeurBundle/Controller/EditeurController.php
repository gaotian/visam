<?php

namespace EditeurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class EditeurController extends Controller
{

    /**
     * Accueil de l'éditeur
     *
     * @Route("editeur", name="editeur")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        //récupérer l'année de la collecte active
        $annee = $em->getRepository('AppBundle:Collecte')->findCollecteActive();

        if(!$annee){
            $this->addFlash('success', "Il n'y a pas de collecte active, vous êtes redirigé sur la page éditeur où vous pouvez modifier les données de la dernière collectec clôturée.");
            return $this->redirectToRoute('search');
        }

        $annee = $annee[0]['annee'];

        $user = $this->getUser();
        $userId = $user->getId();

        if ($user->hasRole('ROLE_ADMIN')){
            $etablissements = $em->getRepository('AppBundle:Etablissement')->findAllByCollecteActive();
            $laboratoires = $em->getRepository('AppBundle:Etablissement')->findAllLabosByCollecte($annee, $userId);
            $formations = $em->getRepository('AppBundle:Etablissement')->findAllFormationsByCollecte($annee);
            $eds = $em->getRepository('AppBundle:Etablissement')->findAllEdsByCollecte($annee);
        }

        else {
            $etablissements = $em->getRepository('AppBundle:Etablissement')->findAllByOneContributeur($userId);
            $laboratoires = $em->getRepository('AppBundle:Etablissement')->findAllLabosByCollecteAndUser($annee, $userId);
            $formations = $em->getRepository('AppBundle:Etablissement')->findAllFormationsByCollecteAndUser($annee, $userId);
            $eds = $em->getRepository('AppBundle:Etablissement')->findAllEdsByCollecteAndUser($annee, $userId);
        }

        return $this->render('EditeurBundle:Editeur:index.html.twig', array(
            'user' => $user,
            'etablissements' => $etablissements,
            'formations' => $formations,
            'laboratoires' => $laboratoires,
            'eds' => $eds,
            'annee_collecte' => $annee
        ));
    }

}
