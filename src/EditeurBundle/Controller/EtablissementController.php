<?php

namespace EditeurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;

use AppBundle\Entity\Etablissement;
use EditeurBundle\Form\EtablissementType;

/**
 *
 * @Route("/admin/etablissement")
 */
class EtablissementController extends Controller
{

    /**
     * Créer un établissement
     *
     * @Route("/new", name="editeur_etablisement_new")
     */
    public function newAction(Request $request){

        $etablissement = new Etablissement();

        $form = $this->createForm(EtablissementType::class, $etablissement);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $etablissement = $form->getData();

            $etablissement->setActive(true);

            $em->persist($etablissement);
            $em->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render('EditeurBundle:Etablissement:new.html.twig', array(
            'etablissementForm' => $form->createView()
        ));
    }

    /**
     * @Route("/{id}/edit" , name = "editeur_etablissement_edit")
     */
    public function editAction(Request $request, Etablissement $etablissement, $id){

        $etablissementForm = $this->createForm('EditeurBundle\Form\EtablissementType', $etablissement);
        $etablissementForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $etablissement = $em->getRepository('AppBundle:Etablissement')->findOneByEtablissementId($id);

        if ($etablissementForm->isSubmitted() && $etablissementForm->isValid()){

            $etablissement = $etablissementForm->getData();
            $now = new \DateTime();
            $etablissement->setLastUpdate($now);

            $em->persist($etablissement);
            $em->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render('EditeurBundle:Etablissement:edit.html.twig', array(
            'etablissementForm' => $etablissementForm->createView(),
            'etablissement' => $etablissement
        ));
    }


    /**
     * @Route("/delete/{etablissementId}", name="editeur_etablissement_ajax_delete")
     * @Method("DELETE")
     */
    public function deleteAjaxAction($etablissementId)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if ($user->hasRole('ROLE_USER')){
            $this->handlerDelete($em, $etablissementId);
        }

        return new Response(null, 204);

    }

    private function handlerDelete($em, $etablissementId){

        $query = $em->createQuery(
            'SELECT l FROM AppBundle:Labo l INNER JOIN l.etablissement e WHERE e.etablissementId = :id AND l.id NOT IN (
                SELECT l2.id FROM AppBundle:Labo l2 INNER JOIN l2.etablissement e2 GROUP BY l2.id HAVING COUNT(l2.id)  > 1
            )'
        );
        $query->setParameter('id', $etablissementId);
        $labos =$query->getResult();
        foreach ($labos as $labo){
            $em->remove($labo);
        }

        $query = $em->createQuery(
            'SELECT l FROM AppBundle:Formation l INNER JOIN l.etablissement e WHERE e.etablissementId = :id AND l.id NOT IN (
                SELECT l2.id FROM AppBundle:Formation l2 INNER JOIN l2.etablissement e2 GROUP BY l2.id HAVING COUNT(l2.id)  > 1
            )'
        );
        $query->setParameter('id', $etablissementId);
        $formations = $query->getResult();
        foreach ($formations as $formation){
            $em->remove($formation);
        }

        $query = $em->createQuery(
            'SELECT l FROM AppBundle:Ed l INNER JOIN l.etablissement e WHERE e.etablissementId = :id AND l.edId NOT IN (
                SELECT l2.edId FROM AppBundle:Ed l2 INNER JOIN l2.etablissement e2 GROUP BY l2.edId HAVING COUNT(l2.edId)  > 1
            )'
        );
        $query->setParameter('id', $etablissementId);
        $eds = $query->getResult();
        foreach ($eds as $ed){
            $em->remove($ed);
        }

        $etablissement = $em->getRepository('AppBundle:Etablissement')
        ->find($etablissementId);
        $em->remove($etablissement);

        $em->flush();
    }

}





