<?php

namespace EditeurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Formation;

/**
 *
 * @Route("/editeur/formation")
 */
class FormationController extends Controller
{
    /**
     * Créer une formation
     *
     * @Route("/new", name="editeur_formation_new")
     */
    public function newFormationAction(Request $request){

        $formation = new Formation();

        $em = $this->getDoctrine()->getManager();

        //ajout des établissements pour le formulaire
        $user = $this->getUser();

        //si l'utilisateur est administrateur, on lui ajoute tous les établissements
        if ($user->hasRole('ROLE_ADMIN')){
            $query = $em->createQuery(
                'SELECT e.etablissementId as id FROM AppBundle:Etablissement e'
            );
        }

        //sinon, on lui conditionne un établissement
        else{

            $userId = $user->getId();
            $query = $em->createQuery(
                'SELECT e.etablissementId as id FROM AppBundle:User u INNER JOIN u.etablissement e WHERE u.id = :user'
            );
            $query->setParameter('user', $userId);
        }
        $etablissements = $query->getResult();

        $query = $em->createQuery(
            'SELECT l.localisationId as id FROM AppBundle:Localisation l JOIN l.etablissement as e WHERE e.etablissementId IN (:etablissements)'
        );
        $query->setParameter('etablissements', $etablissements);
        $localisations = $query->getResult();

        $form = $this->createForm('EditeurBundle\Form\FormationType', $formation, array(
            'etablissements' => $etablissements,
            'localisations' => $localisations
        ));

        $year = $em->getRepository('AppBundle:Collecte')->findDerniereCollecte();
        $year = $year[0]['annee'];

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $formation = $form->getData();
            $em = $this->getDoctrine()->getManager();

            if(count($localisations) == 1){

                $repository = $this->getDoctrine()->getRepository('AppBundle:Localisation');
                $localisation = $repository->findOneByLocalisationId($localisations[0]['id']);

                $formation->addlocalisation($localisation);
            }

            $formation->setAnneeCollecte($year);

            $now = new \DateTime();
            $formation->setDateCreation($now);
            $formation->setLastUpdate($now);

            $em->persist($formation);
            $em->flush();

            //Création et set de l'objetId
            $lastId = $formation->getId();
            $formation->setObjetId("F".$lastId);

            $em->persist($formation);
            $em->flush();

            $this->addFlash(
                'success',
                "Une nouvelle formation a bien été créée!"
            );

            return $this->redirectToRoute('editeur');
        }

        return $this->render('EditeurBundle:Formation:new.html.twig', array(
            'edit_form' => $form->createView(),
            'formation' => $formation,
            'etablissements' => $etablissements,
            'localisations' => $localisations,
            'year' => $year
        ));
    }


    /**
     * Editer une formation
     *
     * @Route("/{id}/edit", name="editeur_formation_edit")
     */
    public function editFormationAction(Request $request, Formation $formation){

        $deleteForm = $this->createDeleteForm($formation);
        $em = $this->getDoctrine()->getManager();

        //ajout des établissements pour le formulaire
        $user = $this->getUser();

        if ($user->hasRole('ROLE_ADMIN')){
            $query = $em->createQuery(
                'SELECT e.etablissementId as id FROM AppBundle:Etablissement e'
            );
        }

        else{
            $userId = $user->getId();
            $query = $em->createQuery(
                'SELECT e.etablissementId as id FROM AppBundle:User u INNER JOIN u.etablissement e WHERE u.id = :user'
            );
            $query->setParameter('user', $userId);
        }
        $etablissements = $query->getResult();


        //vérification que l'utilisateur peut modifier cette formation

        //Sélection de tous les établissements rattachés à la formation
        $query = $em->createQuery("SELECT e.etablissementId as id FROM AppBundle:Etablissement e JOIN e.formation f WHERE f.id = :id");
        $query->setParameter('id', $formation->getId());
        $etab_user = $query->getResult();

        //vérification que les établissement de la formation sont bien dans ceux du user

        $checkUser = [];

        if ($user->hasRole('ROLE_ADMIN')){
            $checkUser = ['all'];
        }
        else{
            for ($i = 0; $i < count($etablissements); $i++){

                for($j = 0; $j < count($etab_user);$j++){
                    if($etablissements[$i] == $etab_user[$j]){
                        array_push($checkUser,$etab_user[$j]);
                    }

                }
            }
        }

        //si l'utilisateur a l'établissement de la formation dans sa liste
        if(count($checkUser) > 0) {

            $query = $em->createQuery(
                'SELECT l.localisationId as id FROM AppBundle:Localisation l JOIN l.etablissement as e WHERE e.etablissementId IN (:etablissements)'
            );
            $query->setParameter('etablissements', $etablissements);
            $localisations = $query->getResult();

            $editForm = $this->createForm('EditeurBundle\Form\FormationType', $formation, array(
                'etablissements' => $etablissements,
                'localisations' => $localisations
            ));

            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $now = new \DateTime();
                $formation->setLastUpdate($now);

                $em->persist($formation);
                $em->flush();

                $this->addFlash(
                    'success',
                    "Les changements ont été sauvegardés!"
                );

                return $this->redirectToRoute('formation', array('id' => $formation->getId()));

            }

            return $this->render('EditeurBundle:Formation:edit.html.twig', array(
                'formation' => $formation,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                'etablissements' => $etablissements,
                'localisations' => $localisations
            ));
        }
        else{
            $this->addFlash('success', "Vous ne pouvez modifier cette formation, vous n'êtes pas rattaché à l'établissement auquelle elle appartient");
            return $this->redirectToRoute('formation', array('id' => $formation->getId()));
        }
    }

    /**
     * Fonction pour effacer via ajax une formation
     *
     * @Route("/delete/{id}", name="editeur_formation_ajax_delete")
     * @Method("DELETE")
     */
    public function deleteAjaxAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if ($user->hasRole('ROLE_USER')){

            /** @var Formation $formation */
            $formation = $em->getRepository('AppBundle:Formation')
                ->find($id);
            $em->remove($formation);
            $em->flush();
        }

        return new Response(null, 204);

    }

    /**
     * Effacer une formation
     *
     * @Route("/{id}/delete", name="editeur_formation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Formation $formation)
    {
        $form = $this->createDeleteForm($formation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($formation);
            $em->flush();
        }

        return $this->redirectToRoute('editeur');
    }

    /**
     * Créer un form pour effacer une formation
     *
     * @param Formation $formation
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm(Formation $formation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('editeur_formation_delete', array('id' => $formation->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}
